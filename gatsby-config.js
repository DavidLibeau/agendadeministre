/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [
    'gatsby-plugin-netlify',
    'gatsby-theme-material-ui',
    'gatsby-source-agendadeministre',
    'gatsby-plugin-agendadeministre',
    {
      resolve: 'gatsby-transformer-json',
      options: {
        typeName: ({ node: { sourceInstanceName } }) => sourceInstanceName,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'events',
        path: `${__dirname}/data/agendas/`,
      },
    },
  ],
};
