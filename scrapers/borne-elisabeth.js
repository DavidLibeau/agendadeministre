var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'borne-elisabeth';
var minId = 'elisabeth-borne';

var scrap = function () {
	return minEcoloSolidaireScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
