import React from 'react';
import clsx from 'clsx';
import { graphql, Link } from 'gatsby';
import { Grid, Paper, Avatar, Typography, makeStyles } from '@material-ui/core';
import Layout from '../components/Layout';

const useStyles = makeStyles(theme => ({
  membersGrid: {
    marginTop: theme.spacing(2),
  },
  card: {
    display: 'flex',
    alignItems: 'stretch',
    justifyContent: 'stretch',
  },
  link: {
    display: 'block',
    width: '100%',
    padding: theme.spacing(2),
    textDecoration: 'none',
  },
  linkDisabled: {
    opacity: 0.25,
  },
  avatar: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    marginRight: theme.spacing(1),
  },
  qualite: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  gouvCalendar: {
    margin: '20px 0'
  }
}));

const Home = ({ data: { allGouvMember: { gouvMembers = [] } = {} } = {} }) => {
  const classes = useStyles();

  return (
    <Layout>
      <Grid container alignItems="center" className={classes.header}>
        <Grid item>
          <Typography variant="h1">
            AgendaDeMinistre.fr
          </Typography>
          <Typography variant="subtitle1">
            Vous avez toujours rêvé d'avoir un agenda de ministre ?
          </Typography>
        </Grid>
      </Grid>

      <Grid
        container
        spacing={2}
        alignItems="stretch"
        className={classes.membersGrid}
      >
        {gouvMembers.map(gouvMember => {
          const { nom, prenom, uid, photo, civilite, qualite, dispo } = gouvMember;
          return (
            <Grid item className={classes.card} xs={3}>
              <Paper
                className={clsx(classes.link, { [classes.linkDisabled]: !dispo })}
                key={uid}
                component={Link}
                to={uid}
              >
                <Grid container alignItems="center" wrap="nowrap">
                  <Grid item>
                    <Avatar alt="" src={photo} className={classes.avatar}/>
                  </Grid>
                  <Grid item>
                    <Typography variant="body1">{civilite} {prenom} {nom}</Typography>
                  </Grid>
                </Grid>

                <Typography variant="caption" className={classes.qualite}>{qualite}</Typography>
              </Paper>
            </Grid>
          );
        })}
      </Grid>
      
      <Grid
        container
        className={classes.gouvCalendar}
      >
        <Paper
          className={classes.link}
          component={Link}
          to="/gouv"
        >
          <Grid container alignItems="center">
            <Grid item>
              <Avatar alt="" className={classes.avatar} />
            </Grid>
            <Grid item>
              <Typography variant="body1">Tout le gouvernement</Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Layout>
  );
};

export default Home;

export const query = graphql`
  query {
    allGouvMember(sort: {fields: nom}) {
      gouvMembers: nodes {
        nom
        prenom
        qualite
        photo
        uid
        civilite
        dispo
      }
    }
  }`;
