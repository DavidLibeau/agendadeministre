var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
moment.locale('fr');

var scrap = async function (calendarId, baseLink) {
	var calendar = [];
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var now = moment();
		if (now.day() == 0) {
			var premierJourDeLaSemaine = now.subtract(1, 'weeks').day(1);
		} else {
			var premierJourDeLaSemaine = now.day(1);
		}
		var weekKey = premierJourDeLaSemaine.format("DDMMYYYY");

		const resList = await fetch.simple(baseLink);

		var dataList = await resList;

		const {
			JSDOM
		} = require("jsdom");
		const {
			window
		} = new JSDOM(dataList);
		const $l = require("jquery")(window);

		var currentDay = "";
		var i = 0;
		var total = $l(".linkBloc .readmore a").length;

		$l(".linkBloc .readmore a").each(async function (index, el) {
			const res = await fetch.nSave('https://www.interieur.gouv.fr' + $l(this).attr('href'), calendarId + '-' + $l(this).attr('href').split('/').pop() + '.txt');

			var data = await res;

			const {
				JSDOM
			} = require("jsdom");
			const {
				window
			} = new JSDOM(data);
			const $ = require("jquery")(window);

			var currentDay = "";
			var j = 0;
			$(".attribute-text").children().each(function () {
				if ($(this).prop("tagName") == "H2") {
					currentDay = $(this).text();
				} else {
					if ($(this).children("b").length == 1) {
						var startDate = moment(currentDay + " " + $(this).children("b").first().text(), "dddd D MMMM hh\hmm");
						var endDate = moment(currentDay + " " + $(this).children("b").first().text(), "dddd D MMMM hh\hmm").add(1, 'h');
						$(this).children("b").remove();
						var content = $(this).text().trim();
						if (content.split(" - ").pop()) {
							var location = content.split(" - ").pop().trim();
							content = content.replace(" - " + content.split(" - ").pop().trim(), "");
						}
						calendar.push({
							id: calendarId + "-" + j + "-" + i,
							calendarId: calendarId,
							title: content,
							category: 'time',
							start: startDate.format(),
							end: endDate.format(),
							location: location,
							attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
						});
					}
					j++;
				}
			});
			i++;
			if (total == i) {
				//console.log(calendar);

				var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
				fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
					if (err) {
						return console.log(calendarId + " " + err);
					} else {
						if (global.env == "dev") {
							console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
						}
					}

				});
			}
		});
	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;
