var minEcoloSolidaireScrapper = require('./common/min-ecologique-solidaire');

var calendarId = 'djebbari-jean-baptiste';
var minId = 'jean-baptiste-djebbari';

var scrap = function () {
	return minEcoloSolidaireScrapper.scrap(calendarId, minId);
}

exports.scrap = scrap;
