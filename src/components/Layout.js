import React from 'react';
import clsx from 'clsx';
import Helmet from 'react-helmet';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';

const useStyles = makeStyles({
  main: {
    padding: '1em',
    height: '100vh',
  },
});

const Layout = ({ className, title = '', ...props }) => {
  const classes = useStyles();

  return (
    <Container maxWidth="lg">
      <Helmet
        htmlAttributes={{ lang: 'fr' }}
        title={title}
        titleTemplate="%s | AgendaDeMinistre"
      />
      <div
        className={clsx(classes.main, className)}
        {...props}
      />
    </Container>
  );
};

export default Layout;
