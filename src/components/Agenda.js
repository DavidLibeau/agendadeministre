import React from 'react';
import { graphql, Link } from 'gatsby';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { Typography, Avatar, makeStyles, Grid, Link as MuiLink } from '@material-ui/core';
import { ArrowBack, Event } from '@material-ui/icons';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'moment/locale/fr';

import Layout from './Layout';

const localizer = momentLocalizer(moment);
const accessor = key => ({ [key]: k }) => moment(k).toDate();
const startAccessor = accessor('start');
const endAccessor = accessor('end');
const useStyles = makeStyles(theme => ({
  header: {
    marginTop: theme.spacing(4),
  },
  icon: {
    marginBottom: '-5px',
    marginRight: '5px'
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    marginRight: theme.spacing(2),
  },
  links: {
    marginLeft: 'auto',
    alignSelf: 'flex-end',
  },
  calendar: {
    marginTop: theme.spacing(4),
    height: 800,
  },
}));

const Agenda = props => {
  const { data: {
    allEvents: { events = [] } = {},
    gouvMember: { nom, prenom, civilite, qualite, photo, uid } = {},
  } = {} } = props;

  const classes = useStyles();

  return (
    <Layout>
      <MuiLink component={Link} to="/"><ArrowBack fontSize="small" className={classes.icon}/>
        Retour à la liste</MuiLink>

      <Grid container alignItems="center" className={classes.header}>
        <Grid item>
          <Avatar
            alt=""
            src={photo}
            className={classes.avatar}
          />
        </Grid>
        <Grid item>
          <Typography variant="h1">
            {civilite} {prenom} {nom}
          </Typography>
          <Typography variant="subtitle1">
            {qualite}
          </Typography>
        </Grid>
        <Grid item className={classes.links}>
          <MuiLink href={`/${uid}.ics`}>
            <Event fontSize="small" className={classes.icon}/>
            S'abonner : <code>http://agendadeministre.fr/{uid}.ics</code>
          </MuiLink>
        </Grid>
      </Grid>

      <Calendar
        className={classes.calendar}
        localizer={localizer}
        events={events}
        startAccessor={startAccessor}
        endAccessor={endAccessor}
        popup
        defaultView="week"
        min={moment('08:00', 'HH:mm').toDate()}
        max={moment('20:00', 'HH:mm').toDate()}
      />
    </Layout>
  );
};

export default Agenda;

export const query = graphql`
  query ($uid: String) {
    allEvents(filter: { calendarId: { eq: $uid } }) {
      events: nodes {
        title
        category
        start
        end
        location
        attendees
      }
    }
    gouvMember(uid: {eq: $uid}) {
      nom
      prenom
      photo
      qualite
      civilite
      uid
    }
  }
`;
