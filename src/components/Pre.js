import React from 'react';

const Pre = props => (
  <pre>
    {JSON.stringify(props, null, 2)}
  </pre>
);

export default Pre;
