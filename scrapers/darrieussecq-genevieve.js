var minDefenseScrapper = require('./common/min-defense');

var calendarId = 'darrieussecq-genevieve';
var rssLink = 'https://www.defense.gouv.fr/dynrss/feed/523776';

var scrap = function () {
	return minDefenseScrapper.scrap(calendarId, rssLink);
}

exports.scrap = scrap;
