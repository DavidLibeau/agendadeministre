import React from 'react';
import { graphql, Link } from 'gatsby';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { Typography, Avatar, Icon, makeStyles, Grid, Link as MuiLink } from '@material-ui/core';
import { ArrowBack, Event } from '@material-ui/icons';

import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'moment/locale/fr';

import Layout from '../components/Layout';

const localizer = momentLocalizer(moment);
const accessor = key => ({ [key]: k }) => moment(k).toDate();
const startAccessor = accessor('start');
const endAccessor = accessor('end');
const useStyles = makeStyles(theme => ({
  header: {
    marginTop: theme.spacing(4),
  },
  icon: {
    marginBottom: '-5px',
    marginRight: '5px'
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
    marginRight: theme.spacing(2),
  },
  links: {
    marginLeft: 'auto',
    alignSelf: 'flex-end',
  },
  calendar: {
    marginTop: theme.spacing(4),
    height: 800,
  },
}));

const Agenda = props => {
  const { data: {
    allEvents: { events = [] } = {},
  } = {} } = props;

  const classes = useStyles();

  return (
    <Layout>
      <MuiLink component={Link} to="/"><ArrowBack fontSize="small" className={classes.icon}/>
        Retour à la liste</MuiLink>

      <Grid container alignItems="center" className={classes.header}>
        <Grid item>
          <Avatar alt="" className={classes.avatar} />
        </Grid>
		<Grid item>
          <Typography variant="h1">
            Gouvernement
          </Typography>
          <Typography variant="subtitle1">
            Le président, tous les ministres et sécrétaires d'État
          </Typography>
        </Grid>
		<Grid item className={classes.links}>
          <MuiLink href={`/gouv.ics`}>
            <Event fontSize="small" className={classes.icon}/>
            S'abonner : <code>http://agendadeministre.fr/gouv.ics</code>
          </MuiLink>
        </Grid>
      </Grid>

      <Calendar
        className={classes.calendar}
        localizer={localizer}
        events={events}
        startAccessor={startAccessor}
        endAccessor={endAccessor}
        popup
        defaultView="week"
        min={moment('08:00', 'HH:mm').toDate()}
        max={moment('20:00', 'HH:mm').toDate()}
      />
    </Layout>
  );
};

export default Agenda;

export const query = graphql`
  query {
    allEvents {
      events: nodes {
        title
        category
        start
        end
        location
        attendees
      }
    }
  }
`;
