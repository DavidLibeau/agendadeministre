var openDataSoftScrapper = require('./common/scrap-OpenDataSoft');

var calendarId = 'blanquer-jean-michel';
var dataLink = 'https://data.education.gouv.fr/explore/dataset/fr-en-agenda-ministre-education-nationale/download?format=json&timezone=Europe/Berlin&use_labels_for_header=true';

var scrap = function () {
	return openDataSoftScrapper.scrap(calendarId, dataLink);
}

exports.scrap = scrap;
