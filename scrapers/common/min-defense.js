var fetch = require('../../lib/fetch-n-save');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
moment.locale('fr');
var xmlParser = require('fast-xml-parser');

var scrap = async function (calendarId, rssLink, separator = ":") {
	var calendar = [];
	try {
		var gouv = [];
		var gouvSearch = {};
		var filePath = path.join(process.cwd(), 'data', 'gouv.json');
		const gouvData = await fs.promises.readFile(filePath);
		gouv = JSON.parse(gouvData);
		for (g in gouv) {
			gouvSearch[gouv[g].id] = gouv[g];
		}

		var dataList = await fetch.fakeBrowser.simple(rssLink);
		dataList = dataList.replace(/<description>/gm, "");
		dataList = dataList.replace(/<\/description>/gm, "");
		dataList = dataList.replace(/<enclosure url="" length="" type="">/gm, "");
		dataList = dataList.replace(/<\/enclosure>/gm, "");
		dataList = dataList.replace(/<pubdate>/gm, "</link><pubdate>");
		// Pire flux xml que j'ai jamais vu. Une prouesse.
		var parsedXml = await xmlParser.parse(dataList);
		var items = parsedXml['html']['body']['rss']['channel']['item'];
		items = items.slice(0, 10);

		var currentDay = "";
		var i = 0;
		var total = items.length;

		for (item in items) {
			i++;
			item = items[item];
			if (item['link'].search("\n") != -1) {
				item['link'] = item['link'].split("\n")[0];
			}

			const res = await fetch.fakeBrowser.nSave(item['link'], calendarId + '-' + item['title'] + '.txt');

			var data = await res;

			const {
				JSDOM
			} = require("jsdom");
			const {
				window
			} = new JSDOM(data);
			const $ = require("jquery")(window);

			var currentDay = "";
			var j = 0;
			$(".ezxmltext-field").children().each(function () {
				var strong = $(this).find("strong").first().text().trim().replace("2020", "");
				$(this).find("strong").first().remove();
				if ($(this).text().length == 0) {
					currentDay = strong;
				} else {
					var content = '';
					if(strong){
						 content = strong;
					}
					content += $(this).text().replace(/&nbsp;/g, " ");
					var time = false;
					if (content.search("\n") != -1) {
						content = content.split("\n")[0];
					}
					if (content.indexOf(":") != -1 && content.indexOf(":") < 10) {
						time = content.split(":")[0];
					}else if (content.indexOf("-") != -1 && content.indexOf("-") < 10) {
						time = content.split("-")[0];
					} else if (content.indexOf("–") != -1 && content.indexOf("–") < 10) {
						time = content.split("–")[0];
					} else if (content.indexOf("–") != -1 && content.indexOf("–") < 10) {
						time = content.split("–")[0];
					} else {
						//console.log("Error : " + content);
					}
					if (time) {
						content = content.replace(time, "");
						content = content.slice(2, content.length);
						var location = '';
						if (content.lastIndexOf("-") > 20 && content.charAt(content.lastIndexOf("-")+1)==" ") {
							location = content.split("-").pop().trim();
						} else if (content.lastIndexOf("-") > 20 && content.charAt(content.lastIndexOf("-")+1)==" ") {
							location = content.split("-").pop().trim();
						} else if (content.lastIndexOf("–") > 20 && content.charAt(content.lastIndexOf("–")+1)==" ") {
							location = content.split("–").pop().trim();
						}
						if (location != '') {
							content = content.replace(location, "");
							content = content.slice(0, content.length - 3);
						}
						if (time.length < 4) {
							time += "00";
						}
						var startDate = moment(currentDay + " " + time, "dddd D MMMM hh\hmm");
						var endDate = moment(currentDay + " " + time, "dddd D MMMM hh\hmm").add(1, 'h');
						if (startDate.isValid() && endDate.isValid()) {
							if (startDate.hour() == 0) {
								//console.log($(this).html())
								//console.log(currentDay)
								//console.log(strong)
							} else {
								calendar.push({
									id: calendarId + "-" + j + "-" + i,
									calendarId: calendarId,
									title: content,
									category: 'time',
									start: startDate.format(),
									end: endDate.format(),
									location: location,
									attendees: [gouvSearch[calendarId].prenom + " " + gouvSearch[calendarId].nom]
								});
							}
						}
					}
				}
				j++;
			});

			if (total == i) {
				//console.log(calendar);

				var filePath = path.join(process.cwd(), 'data', 'agendas', calendarId + '.json');
				fs.writeFile(filePath, JSON.stringify(Object.values(calendar), null, 2), function (err) {
					if (err) {
						return console.log(calendarId + " " + err);
					} else {
						if (global.env == "dev") {
							console.log("[" + calendarId + "] : " + calendar.length + " events saved !");
						}
					}

				});
			}
		}
	} catch (err) {
		console.error(err);
	}
}

exports.scrap = scrap;
