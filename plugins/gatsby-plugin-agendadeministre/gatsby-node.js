const console = require('../../lib/log-n-save');
const path = require('path');
const moment = require('moment');
const ical = require('ical-generator');

exports.createPages = async ({
  graphql,
  actions: { createPage },
}) => {
  const queryResults = await graphql(`
    query {
      allEvents {
        events: nodes {
          calendarId
          start
          end
          title
          attendees
          internal {
            contentDigest
          }
        }
      }
      allGouvMember {
        gouvMembers: nodes {
          nom
          prenom
          qualite
          uid
          civilite
        }
      }
    }
  `);

  const {
    data: {
      allEvents: { events = [] } = {},
      allGouvMember: { gouvMembers = [] } = {},
    } = {},
  } = queryResults;

  const min = moment(new Date()).subtract(3, 'months');

  // Keep only event from three months past
  const recentEvents = events.filter(({ start }) => moment(start, 'YYYY-MM').isAfter(min));

  /**
   * Create page for each event
   */
  recentEvents
    .forEach(({ calendarId, internal: { contentDigest } = {} }) => {
      createPage({
        path: path.join(calendarId, contentDigest),
        component: path.resolve('src/components/Pre.js'),
        context: { contentDigest },
      });
    });
  
  /**
   * Create .ics for gouvMembers
   */
  gouvMembers
    .forEach(({ uid, civilite, nom, prenom, qualite }) => {
      const memberEvents = recentEvents.filter(({ calendarId }) => calendarId === uid);
      const calendar = ical({
        domain: 'agendadeministre.fr',
        name: `Agenda de ${civilite} ${prenom} ${nom} (${qualite})`,
      });

      memberEvents.forEach(({ start, end, title: summary, location, attendees }) => {
        try {
          calendar.createEvent({
            start,
            end,
            summary,
            location,
            attendees: attendees.map(name => ({ email: 'noreply@agendadeministre.fr', name })),
          });
        } catch (e) {
          console.log(e);
        }
      });

      calendar.saveSync(path.join('public', `${uid}.ics`));
    });
  
  /**
   * Create gouv.ics
   */
  const calendar = ical({
    domain: 'agendadeministre.fr',
    name: `Agenda du gouvernement`,
  });
  recentEvents.forEach(({ start, end, title: summary, location, attendees }) => {
    try {
      calendar.createEvent({
        start,
        end,
        summary,
        location,
        attendees: attendees.map(name => ({ email: 'noreply@agendadeministre.fr', name })),
      });
    } catch (e) {
      console.log(e);
    }
  });

  calendar.saveSync(path.join('public', `gouv.ics`));
};
