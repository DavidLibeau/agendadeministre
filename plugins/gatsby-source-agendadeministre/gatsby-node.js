const fs = require('fs').promises;
const path = require('path');

exports.sourceNodes = async ({
  actions: { createPage, createNode },
  createNodeId,
  createContentDigest,
}) => {
  const gouvBuffer = await fs.readFile(path.join(process.cwd(), 'data', 'gouv.json'));
  const gouvJson = gouvBuffer.toString();
  const gouv = JSON.parse(gouvJson);

  gouv.forEach(gouvMember => {
    const uid = gouvMember.id;
    const id = createNodeId(`gouv-member-${uid}`);

    const node = {
      ...gouvMember,
      uid,
      id,
      parent: null,
      children: [],
      internal: {
        type: 'GouvMember',
        content: JSON.stringify(gouvMember),
        contentDigest: createContentDigest(gouvMember),
      },
    };

    createNode(node);

    createPage({
      path: gouvMember.id,
      component: path.resolve('src/components/Agenda.js'),
      context: { id, uid },
    });
  });
};
